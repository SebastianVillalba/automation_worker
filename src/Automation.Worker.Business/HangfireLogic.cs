﻿using Microsoft.Extensions.Configuration;
using Hangfire;
using StackExchange.Redis;
using Hangfire.Redis;
using Automation.Worker.Entities;

namespace Automation.Worker.Business
{
    public class HangfireLogic
    {
        public IConnectionMultiplexer RedisConnection { get; set; } = null!;
        public BackgroundJobServer _backgroundJobServer = null!;
        public HangfireLogic()
        {
            RedisConnection = RedisConnectionFactory.RedisMultiplexer;
        }


        public void ConfigurateServer()
        {
            var redisConfig = new RedisStorageOptions();
            redisConfig.Prefix = "hangfire-automation-worker";

            GlobalConfiguration.Configuration.UseRedisStorage(RedisConnection);
        }

        public void StartServer()
        {
            this.ConfigurateServer();
            this._backgroundJobServer = new BackgroundJobServer();
        }
        public void DisposeServer()
        {
            this._backgroundJobServer.Dispose();
        }

        public bool ScheduleJob(Action metodo) //IJob?
        {
            return false;
        }

        public bool RemoveJob(Action metodo) //IJob?
        {
            return false;
        }
    }
}