﻿using Microsoft.EntityFrameworkCore;

namespace Automation.Worker.Entities
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
            : base()
        {

        }
    }

    public class RepositoryContextBuilder
    {
        public static RepositoryContext CreateContext(int accountAutoId)
        {
            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseMySQL(ShardMapping.GetStrConnection(accountAutoId));

            return new RepositoryContext(optionsBuilder.Options);
        }
    }

}