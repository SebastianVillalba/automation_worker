﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Worker.Entities.Modelos
{
    public class ShardInfo
    {
        public int ConnectionAutoID { get; set; }
        public int ShardStart { get; set; }
        public int ShardEnd { get; set; }
        public string MasterConnection { get; set; } = null!;
    }
}
