﻿using System;
using Automation.Worker.Entities.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Automation.Worker.Entities.Context
{
    public partial class biflows_Context : DbContext
    {
        public biflows_Context()
        {
        }

        public biflows_Context(DbContextOptions<biflows_Context> options)
            : base(options)
        {
        }

        public virtual DbSet<ActivityBounce> ActivityBounces { get; set; }
        public virtual DbSet<ActivityClick> ActivityClicks { get; set; }
        public virtual DbSet<ActivityGroup> ActivityGroups { get; set; }
        public virtual DbSet<ActivityHater> ActivityHaters { get; set; }
        public virtual DbSet<ActivityItem> ActivityItems { get; set; }
        public virtual DbSet<ActivityOpen> ActivityOpens { get; set; }
        public virtual DbSet<ActivityPushNotification> ActivityPushNotifications { get; set; }
        public virtual DbSet<ActivitySent> ActivitySents { get; set; }
        public virtual DbSet<ActivitySm> ActivitySms { get; set; }
        public virtual DbSet<ActivitySubscribe> ActivitySubscribes { get; set; }
        public virtual DbSet<ActivityTag> ActivityTags { get; set; }
        public virtual DbSet<ActivityUnsubscribe> ActivityUnsubscribes { get; set; }
        public virtual DbSet<ActivityViral> ActivityVirals { get; set; }
        public virtual DbSet<AsyncProcess> AsyncProcesses { get; set; }
        public virtual DbSet<AutoContactsAction> AutoContactsActions { get; set; }
        public virtual DbSet<AutoContactsCondition> AutoContactsConditions { get; set; }
        public virtual DbSet<AutoContactsEmail> AutoContactsEmails { get; set; }
        public virtual DbSet<AutoContactsEvent> AutoContactsEvents { get; set; }
        public virtual DbSet<AutoContactsSm> AutoContactsSms { get; set; }
        public virtual DbSet<AutoContactsVolume> AutoContactsVolumes { get; set; }
        public virtual DbSet<AutoQueue> AutoQueues { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<EventosDetalle> EventosDetalles { get; set; }
        public virtual DbSet<EventsTracking> EventsTrackings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseMySQL("server=10.200.1.178;user=admin;database=biflows_0000;password=xZNPdNPsgY;port=3306");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActivityBounce>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_bounce");

                entity.HasIndex(e => new { e.AccountAutoId, e.ActionAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ActivityAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ActivityAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type).HasColumnType("smallint(6) unsigned");
            });

            modelBuilder.Entity<ActivityClick>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_click");

                entity.HasIndex(e => new { e.AccountAutoId, e.ActionAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ActivityAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ActivityAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Item)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.LinkAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("LinkAutoID");

                entity.Property(e => e.Tags)
                    .IsRequired()
                    .HasMaxLength(2500);

                entity.Property(e => e.Type).HasColumnType("smallint(6) unsigned");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ActivityGroup>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_group");

                entity.HasIndex(e => new { e.AccountAutoId, e.GroupAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActivityType)
                    .HasColumnType("smallint(5) unsigned")
                    .HasComment("1=Adhesion\r\n2=Supresion");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.GroupAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("GroupAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<ActivityHater>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_hater");

                entity.HasIndex(e => new { e.AccountAutoId, e.ActionAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ActivityAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ActivityAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type).HasColumnType("smallint(6) unsigned");
            });

            modelBuilder.Entity<ActivityItem>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_item");

                entity.HasIndex(e => new { e.AccountAutoId, e.ItemId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActivityType)
                    .HasColumnType("smallint(6) unsigned")
                    .HasComment("1=Adhesion\r\n2=Supresion");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ItemId)
                    .IsRequired()
                    .HasMaxLength(2500)
                    .HasColumnName("ItemID");
            });

            modelBuilder.Entity<ActivityOpen>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_open");

                entity.HasIndex(e => new { e.AccountAutoId, e.ActionAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ActivityAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ActivityAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type).HasColumnType("smallint(6) unsigned");
            });

            modelBuilder.Entity<ActivityPushNotification>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_push_notification");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActivityDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ContactAutoID");
            });

            modelBuilder.Entity<ActivitySent>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_sent");

                entity.HasIndex(e => new { e.AccountAutoId, e.ActionAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ActivityAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ActivityAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type).HasColumnType("smallint(6) unsigned");
            });

            modelBuilder.Entity<ActivitySm>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_sms");

                entity.HasIndex(e => new { e.AccountAutoId, e.EmbActionAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.HasIndex(e => new { e.AccountAutoId, e.MessageId }, "AccountAutoID_3");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.BulkId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("BulkID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.Count).HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("varchar(5000)");

                entity.Property(e => e.EmbActionAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("EmbActionAutoID");

                entity.Property(e => e.GroupId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("GroupID");

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.IsFirstLog).HasColumnType("tinyint(4)");

                entity.Property(e => e.MessageId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("MessageID");

                entity.Property(e => e.MessageTo)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ActivitySubscribe>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_subscribe");

                entity.HasIndex(e => new { e.AccountAutoId, e.ActionAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ActivityAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ActivityAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type).HasColumnType("smallint(6) unsigned");
            });

            modelBuilder.Entity<ActivityTag>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_tag");

                entity.HasIndex(e => new { e.AccountAutoId, e.TagAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActivityType)
                    .HasColumnType("smallint(6) unsigned")
                    .HasComment("1=Adhesion\r\n2=Supresion");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.TagAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("TagAutoID");
            });

            modelBuilder.Entity<ActivityUnsubscribe>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_unsubscribe");

                entity.HasIndex(e => new { e.AccountAutoId, e.ActionAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ActivityAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ActivityAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type).HasColumnType("smallint(6) unsigned");
            });

            modelBuilder.Entity<ActivityViral>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_viral");

                entity.HasIndex(e => new { e.AccountAutoId, e.ActionAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ActivityAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ActivityAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type).HasColumnType("smallint(6) unsigned");
            });

            modelBuilder.Entity<AsyncProcess>(entity =>
            {
                entity.HasKey(e => e.AsyncProcessAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("async_processes");

                entity.HasIndex(e => new { e.AccountAutoId, e.ItemAutoId, e.ProcessType }, "AccountAutoID");

                entity.Property(e => e.AsyncProcessAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("AsyncProcessAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.CustomProgress).HasMaxLength(255);

                entity.Property(e => e.Deleted).HasColumnType("tinyint(4)");

                entity.Property(e => e.ErrorMessage).HasMaxLength(1000);

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.Open)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PartialProgress).HasColumnType("int(11)");

                entity.Property(e => e.PartialTotal).HasColumnType("int(11)");

                entity.Property(e => e.ProcessDescription).HasColumnType("varchar(5000)");

                entity.Property(e => e.ProcessId)
                    .HasMaxLength(100)
                    .HasColumnName("ProcessID");

                entity.Property(e => e.ProcessStatus).HasColumnType("tinyint(4)");

                entity.Property(e => e.ProcessType).HasColumnType("tinyint(4)");

                entity.Property(e => e.Progress).HasColumnType("int(11)");

                entity.Property(e => e.Total).HasColumnType("int(11)");
            });

            modelBuilder.Entity<AutoContactsAction>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("auto_contacts_actions");

                entity.HasIndex(e => e.ActionAutoId, "ActionAutoID");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ActionData).IsRequired();

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.ExecutedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FlowTraceId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("FlowTraceID");
            });

            modelBuilder.Entity<AutoContactsCondition>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("auto_contacts_conditions");

                entity.HasIndex(e => new { e.ConditionAutoId, e.ContactAutoId }, "EventAutoID");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.ConditionAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ConditionAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.EventData).IsRequired();

                entity.Property(e => e.ExecutedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FlowTraceId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("FlowTraceID");
            });

            modelBuilder.Entity<AutoContactsEmail>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("auto_contacts_emails");

                entity.HasIndex(e => new { e.AccountAutoId, e.EmbActivityAutoId }, "AccountAutoID");

                entity.HasIndex(e => e.ActionAutoId, "ActionAutoID");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.EmbActivityAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("EmbActivityAutoID");

                entity.Property(e => e.EventItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("EventItemAutoID");

                entity.Property(e => e.FlowTraceId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("FlowTraceID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.QueuedById)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("QueuedByID");
            });

            modelBuilder.Entity<AutoContactsEvent>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("auto_contacts_events");

                entity.HasIndex(e => new { e.EventAutoId, e.ContactAutoId }, "EventAutoID");

                entity.HasIndex(e => e.EventData, "EventData");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.EventAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("EventAutoID");

                entity.Property(e => e.EventData)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ExecutedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FlowTraceId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("FlowTraceID");

                entity.Property(e => e.SourceCampaignAutoId)
                    .HasColumnType("int(10)")
                    .HasColumnName("SourceCampaignAutoID");
            });

            modelBuilder.Entity<AutoContactsSm>(entity =>
            {
                entity.HasKey(e => e.ItemAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("auto_contacts_sms");

                entity.HasIndex(e => new { e.AccountAutoId, e.MessageId }, "AccountAutoID");

                entity.HasIndex(e => e.ActionAutoId, "ActionAutoID");

                entity.Property(e => e.ItemAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ItemAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.ActionAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ActionAutoID");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.EventItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("EventItemAutoID");

                entity.Property(e => e.FlowTraceId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("FlowTraceID");

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MessageId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("MessageID");

                entity.Property(e => e.QueuedById)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("QueuedByID");
            });

            modelBuilder.Entity<AutoContactsVolume>(entity =>
            {
                entity.HasKey(e => e.VolumeAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("auto_contacts_volume");

                entity.HasIndex(e => new { e.VolumeYear, e.VolumeMonth, e.AccountAutoId, e.FlowElementAutoId }, "VolumeYear");

                entity.Property(e => e.VolumeAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("VolumeAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.FlowElementAutoId)
                    .HasColumnType("int(11)")
                    .HasColumnName("FlowElementAutoID");

                entity.Property(e => e.FlowElementType).HasColumnType("int(11)");

                entity.Property(e => e.Qtimes)
                    .HasColumnType("int(11)")
                    .HasColumnName("QTimes");

                entity.Property(e => e.VolumeDate).HasColumnType("date");

                entity.Property(e => e.VolumeDay).HasColumnType("int(11)");

                entity.Property(e => e.VolumeMonth).HasColumnType("int(11)");

                entity.Property(e => e.VolumeYear).HasColumnType("int(11)");
            });

            modelBuilder.Entity<AutoQueue>(entity =>
            {
                entity.HasKey(e => e.QueueAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("auto_queue");

                entity.HasIndex(e => e.ActionExecuted, "ActionAutoID");

                entity.HasIndex(e => e.AutoItem, "AutoItem");

                entity.Property(e => e.QueueAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("QueueAutoID");

                entity.Property(e => e.ActionExecuted).HasColumnType("tinyint(4)");

                entity.Property(e => e.AutoItem)
                    .IsRequired()
                    .HasColumnType("varchar(5000)");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.EventAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("EventAutoID");

                entity.Property(e => e.EventBranch)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0 = Rama Executed Event;\r\n1 = Rama NOT Executed Event;");

                entity.Property(e => e.EventItemAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("EventItemAutoID");

                entity.Property(e => e.EventStartDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FlowTraceId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("FlowTraceID");

                entity.Property(e => e.QueuedById)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("QueuedByID");
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.HasKey(e => new { e.ContactAutoId, e.AccountAutoId })
                    .HasName("PRIMARY");

                entity.ToTable("contacts");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(11) unsigned")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(11) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.InsertedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<EventosDetalle>(entity =>
            {
                entity.HasKey(e => e.IdDetalle)
                    .HasName("PRIMARY");

                entity.ToTable("eventos_detalle");

                entity.HasIndex(e => new { e.IdEvento, e.IdCuenta, e.FechaHora }, "idCuenta");

                entity.Property(e => e.IdDetalle)
                    .HasColumnType("int(11)")
                    .HasColumnName("idDetalle");

                entity.Property(e => e.IdCuenta)
                    .HasColumnType("int(11)")
                    .HasColumnName("idCuenta");

                entity.Property(e => e.IdEvento)
                    .HasColumnType("int(11)")
                    .HasColumnName("idEvento");

                entity.Property(e => e.Tracking).HasMaxLength(255);

                entity.Property(e => e.Usuario).HasMaxLength(50);
            });

            modelBuilder.Entity<EventsTracking>(entity =>
            {
                entity.HasKey(e => e.TrackingAutoId)
                    .HasName("PRIMARY");

                entity.ToTable("events_tracking");

                entity.HasIndex(e => new { e.AccountAutoId, e.EnvioAutoId }, "AccountAutoID");

                entity.HasIndex(e => new { e.AccountAutoId, e.ContactAutoId }, "AccountAutoID_2");

                entity.HasIndex(e => new { e.AccountAutoId, e.CustomEventAutoId, e.ExecutedDate }, "AccountAutoID_3");

                entity.HasIndex(e => new { e.AccountAutoId, e.AnonymousAutoId }, "AccountAutoID_4");

                entity.HasIndex(e => new { e.AccountAutoId, e.CustomEventAutoId, e.InsertGuid }, "AccountAutoID_5");

                entity.Property(e => e.TrackingAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("TrackingAutoID");

                entity.Property(e => e.AccountAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("AccountAutoID");

                entity.Property(e => e.AnonymousAutoId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("AnonymousAutoID");

                entity.Property(e => e.Attributes)
                    .IsRequired()
                    .HasColumnType("longtext")
                    .HasColumnName("attributes");

                entity.Property(e => e.Browserx)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("browserx");

                entity.Property(e => e.Browsery)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("browsery");

                entity.Property(e => e.ContactAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("ContactAutoID");

                entity.Property(e => e.CustomEventAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("CustomEventAutoID");

                entity.Property(e => e.EnvioAutoId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("EnvioAutoID");

                entity.Property(e => e.ExecutedDate).HasDefaultValueSql("'1900-01-01 00:00:00'");

                entity.Property(e => e.InsertGuid)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Referrer)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("referrer");

                entity.Property(e => e.Screenx)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("screenx");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("title");

                entity.Property(e => e.UserAgent)
                    .IsRequired()
                    .HasMaxLength(3000)
                    .HasColumnName("userAgent");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
