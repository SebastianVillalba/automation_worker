﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Worker.Entities
{
    internal class ConfigurationGetter
    {

        public static IConfigurationRoot ConfigFile {get; set;}

        static ConfigurationGetter(){

            ConfigFile =  new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json").Build();

        }

        public static T GetParameter<T>(string key)
        {
            return ConfigFile.GetValue<T>(key);
        }

        public static T GetSection<T>(string section)
        {
            var _section = ConfigFile.GetSection(section);
            return _section.Get<T>();    
        }

        public static T GetSection<T>(string section, string subSection)
        {
            var _section = ConfigFile.GetSection(section);
            var _subSection = _section.GetSection(subSection);
            return _subSection.Get<T>();
        }

    }
}
