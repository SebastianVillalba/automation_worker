﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class ActivitySm
    {
        public int ItemAutoId { get; set; }
        public int AccountAutoId { get; set; }
        public int EmbActionAutoId { get; set; }
        public int ContactAutoId { get; set; }
        public DateTime ActivityDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public string MessageId { get; set; }
        public string MessageTo { get; set; }
        public string GroupName { get; set; }
        public int GroupId { get; set; }
        public string Status { get; set; }
        public string BulkId { get; set; }
        public int Count { get; set; }
        public string Description { get; set; }
        public byte IsFirstLog { get; set; }
    }
}
