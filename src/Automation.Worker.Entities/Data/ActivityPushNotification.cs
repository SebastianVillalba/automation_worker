﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class ActivityPushNotification
    {
        public int ItemAutoId { get; set; }
        public int AccountAutoId { get; set; }
        public int ContactAutoId { get; set; }
        public DateTime ActivityDate { get; set; }
    }
}
