﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class AutoQueue
    {
        public int QueueAutoId { get; set; }
        public int ContactAutoId { get; set; }
        public int EventAutoId { get; set; }
        public string AutoItem { get; set; }
        public DateTime ExecutionDate { get; set; }
        public byte ActionExecuted { get; set; }
        public DateTime EventStartDate { get; set; }
        public byte EventBranch { get; set; }
        public int QueuedById { get; set; }
        public int EventItemAutoId { get; set; }
        public long FlowTraceId { get; set; }
    }
}
