﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class AutoContactsAction
    {
        public int ItemAutoId { get; set; }
        public int ContactAutoId { get; set; }
        public int ActionAutoId { get; set; }
        public string ActionData { get; set; }
        public DateTime ExecutedDate { get; set; }
        public long FlowTraceId { get; set; }
    }
}
