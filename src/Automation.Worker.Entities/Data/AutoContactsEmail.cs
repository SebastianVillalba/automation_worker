﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class AutoContactsEmail
    {
        public int ItemAutoId { get; set; }
        public int AccountAutoId { get; set; }
        public int ContactAutoId { get; set; }
        public int ActionAutoId { get; set; }
        public int EmbActivityAutoId { get; set; }
        public int QueuedById { get; set; }
        public int EventItemAutoId { get; set; }
        public DateTime InsertedDate { get; set; }
        public long FlowTraceId { get; set; }
    }
}
