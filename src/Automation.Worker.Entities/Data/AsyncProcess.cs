﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class AsyncProcess
    {
        public int AsyncProcessAutoId { get; set; }
        public string ProcessId { get; set; }
        public byte? ProcessStatus { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public DateTime? DateCancelled { get; set; }
        public int? Total { get; set; }
        public int? Progress { get; set; }
        public string ErrorMessage { get; set; }
        public string CustomProgress { get; set; }
        public byte? ProcessType { get; set; }
        public int? AccountAutoId { get; set; }
        public int? ItemAutoId { get; set; }
        public string ProcessDescription { get; set; }
        public DateTime? PartialDateStart { get; set; }
        public int? PartialTotal { get; set; }
        public int? PartialProgress { get; set; }
        public byte? Open { get; set; }
        public byte Deleted { get; set; }
    }
}
