﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class EventsTracking
    {
        public int TrackingAutoId { get; set; }
        public int AccountAutoId { get; set; }
        public int CustomEventAutoId { get; set; }
        public int EnvioAutoId { get; set; }
        public int ContactAutoId { get; set; }
        public string AnonymousAutoId { get; set; }
        public DateTime ExecutedDate { get; set; }
        public int Browserx { get; set; }
        public int Browsery { get; set; }
        public int Screenx { get; set; }
        public string Referrer { get; set; }
        public string Title { get; set; }
        public string UserAgent { get; set; }
        public string Attributes { get; set; }
        public string InsertGuid { get; set; }
    }
}
