﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class ActivityClick
    {
        public int ItemAutoId { get; set; }
        public int AccountAutoId { get; set; }
        public int ActionAutoId { get; set; }
        public int ActivityAutoId { get; set; }
        public int ContactAutoId { get; set; }
        public DateTime ActivityDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public short Type { get; set; }
        public int LinkAutoId { get; set; }
        public string Url { get; set; }
        public string Tags { get; set; }
        public string Item { get; set; }
    }
}
