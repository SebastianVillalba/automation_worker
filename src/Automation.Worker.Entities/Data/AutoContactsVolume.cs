﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class AutoContactsVolume
    {
        public int VolumeAutoId { get; set; }
        public int? VolumeYear { get; set; }
        public int? VolumeMonth { get; set; }
        public int? VolumeDay { get; set; }
        public DateTime? VolumeDate { get; set; }
        public int? AccountAutoId { get; set; }
        public int? FlowElementAutoId { get; set; }
        public int? FlowElementType { get; set; }
        public int? Qtimes { get; set; }
    }
}
