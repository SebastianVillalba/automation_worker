﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class Contact
    {
        public int ContactAutoId { get; set; }
        public int AccountAutoId { get; set; }
        public string Email { get; set; }
        public DateTime InsertedDate { get; set; }
    }
}
