﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class AutoContactsCondition
    {
        public int ItemAutoId { get; set; }
        public int ContactAutoId { get; set; }
        public int ConditionAutoId { get; set; }
        public DateTime ExecutedDate { get; set; }
        public long FlowTraceId { get; set; }
        public string EventData { get; set; }
    }
}
