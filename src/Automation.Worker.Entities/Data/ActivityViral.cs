﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class ActivityViral
    {
        public int ItemAutoId { get; set; }
        public int AccountAutoId { get; set; }
        public int ActionAutoId { get; set; }
        public int ActivityAutoId { get; set; }
        public int ContactAutoId { get; set; }
        public DateTime ActivityDate { get; set; }
        public short Type { get; set; }
        public DateTime InsertedDate { get; set; }
    }
}
