﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Automation.Worker.Entities.Data
{
    public partial class EventosDetalle
    {
        public int IdDetalle { get; set; }
        public int? IdEvento { get; set; }
        public int? IdCuenta { get; set; }
        public DateTime? FechaHora { get; set; }
        public string Usuario { get; set; }
        public string OtrosDatos { get; set; }
        public string Tracking { get; set; }
    }
}
