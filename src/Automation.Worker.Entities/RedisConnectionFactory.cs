﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.Worker.Services;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;

namespace Automation.Worker.Entities
{
    public class RedisConnectionFactory: IConnector
    {
        public static string Host { get; set; }
        public static  string Port { get; set; }
        public static ConnectionMultiplexer RedisMultiplexer { get; set; }

        static RedisConnectionFactory()
        {
            Host = ConfigurationGetter.GetParameter<string>("Redis:Host");
            Port = ConfigurationGetter.GetParameter<string>("Redis:Port");

            RedisMultiplexer = ConnectionMultiplexer.Connect(
            new ConfigurationOptions
            {
                EndPoints={ Host+":"+Port }
            });
        }

        public static IDatabase getConnection()
        {
            return RedisMultiplexer.GetDatabase();
        }
        
    }
}
