﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.Worker.Entities.Modelos;
using Microsoft.Extensions.Configuration;

namespace Automation.Worker.Entities
{
    public class ShardMapping
    {
        public static int MaxShards { get; set; }
        public static ShardInfo ShardConnection1 { get; set; } = null!;
        public static ShardInfo ShardConnection2 { get; set; } = null!;

        static ShardMapping()
        {
            MaxShards = ConfigurationGetter.GetParameter<int>("shards_mapping:MaxShards");
            ShardConnection1 = ConfigurationGetter.GetSection<ShardInfo>("shards_mapping", "ShardConnection1");
            ShardConnection2 = ConfigurationGetter.GetSection<ShardInfo>("shards_mapping", "ShardConnection2");
        }

        public static string GetStrConnection(int accountAutoID = 0)
        {
            int shardID = 0;
            string strConnection = string.Empty;

            if (accountAutoID > 0)
            {
                shardID = GetAccountShardID(accountAutoID);
            }
            else
            {
                return strConnection;
            }

            if (ShardConnection1.ShardStart <= shardID
                && ShardConnection1.ShardEnd >= shardID)
            {
                strConnection = ShardConnection1.MasterConnection;
            }
            else if (ShardConnection2.ShardStart <= shardID
                 && ShardConnection2.ShardEnd >= shardID)
            {
                strConnection = ShardConnection2.MasterConnection;
            }

            strConnection.Replace("{%DatabaseName%}", "biflows_"+shardID.ToString("#0000"));
            return strConnection;
        }

        public static int GetAccountShardID(int accountAutoID)
        {
            int shardID = accountAutoID % MaxShards;
            return shardID;
        }
    }
}

