﻿using System;
using System.IO;
using Automation.Worker.Business;
using Automation.Worker.Entities.Context;
using Automation.Worker.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DI_Configuration_Logging_ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using ServiceProvider serviceProvider = RegisterServices(args);
            IConfiguration configuration = serviceProvider.GetService<IConfiguration>();
            Prueba();
        }

        private static ServiceProvider RegisterServices(string[] args)
        {
            IConfiguration configuration = SetupConfiguration(args);
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddDbContext<biflows_Context>(item => item.UseMySQL(configuration.GetValue<string>("shards_mapping:ShardConnection1:MasterConnection")));
            serviceCollection.AddDbContext<biflows_Context>(item => item.UseMySQL(configuration.GetValue<string>("shards_mapping:ShardConnection2:MasterConnection")));

            serviceCollection.AddSingleton(configuration);
            serviceCollection.AddScoped<IPrueba, Prueba>();


            return serviceCollection.BuildServiceProvider();
        }

        private static IConfiguration SetupConfiguration(string[] args)
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
        }

        private static void Prueba()
        {
            IPrueba p = 

            p.ProbarContext();
        }
    }
}